## Deploy CloudFront and WAF for an ELB

1.) Run the CloudFormation template

2.) Once the CloudFormation is completed, ensure the CNAME of the DNS is pointed to the CloudFront domain name in route53.

3.) Update the ELB security group ingress rules:

  a.) Navigate to the lambda function named <servicename>-<environment>, select the drop down next to the Test button, then click Test.

  b.) Paste in the following:

  ```
  {
    "Records": [
      {
        "EventVersion": "1.0",
        "EventSubscriptionArn": "arn:aws:sns:EXAMPLE",
        "EventSource": "aws:sns",
        "Sns": {
          "SignatureVersion": "1",
          "Timestamp": "1970-01-01T00:00:00.000Z",
          "Signature": "EXAMPLE",
          "SigningCertUrl": "EXAMPLE",
          "MessageId": "95df01b4-ee98-5cb9-9903-4c221d41eb5e",
          "Message": "{\"create-time\": \"yyyy-mm-ddThh:mm:ss+00:00\", \"synctoken\": \"0123456789\", \"md5\": \"7fd59f5c7f5cf643036cbd4443ad3e4b\", \"url\": \"https://ip-ranges.amazonaws.com/ip-ranges.json\"}",
          "Type": "Notification",
          "UnsubscribeUrl": "EXAMPLE",
          "TopicArn": "arn:aws:sns:EXAMPLE",
          "Subject": "TestInvoke"
        }
      }
    ]
  }
  ```

  c.) Name the test "Update ELB security group with AWS IPs"

  d.) Run the test you just created. It will fail and say it was expecting a different md5 hash value.

  e.) Copy the unexpected value and replace the existing hash value in the test that was created earlier (This is on the "Message" line and the default value is "7fd59f5c7f5cf643036cbd4443ad3e4b").

  f.) Run the test again. It should now succeed and the ingress rules on the ELB security group will be updated with the AWS IPs.

  g.) Verify the security group ingress rules were updated.

4.) Update service file in Kubernetes with security group.

5.) Verify all new ELBs are using the new locked down security group.

Note: External artifact containing code for lambda function is located at upmce-lambda-scripts/elbscip.zip

The security group update is based off this article https://aws.amazon.com/blogs/security/how-to-automatically-update-your-security-groups-for-amazon-cloudfront-and-aws-waf-by-using-aws-lambda/

This script was made great with contributions from the greats Tim Savage, Spencer Conklin, and Ken Howard prior to being forked onto here.
